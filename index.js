const svg = document.getElementById('triangles');

svg.onclick = (e) => {
    const colours = ['red', 'blue', 'green', 'orange', 'pink', 'purple']
    const rando = () => colours[Math.floor(Math.random() * colours.length)]
    document.documentElement.style.cssText = `
    --dark-color: ${rando()};
    --light-color: ${rando()};
    `
}